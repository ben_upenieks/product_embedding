import json, os, random, string
from collections import defaultdict
from multiprocessing import Pool, Process
from matching import tag_to_prod
from PIL import Image
from modelrunner.frameworks.tensorflow import RetinaNetRunner
from modelrunner.frameworks.keras import KerasRunner
from pymongo import MongoClient
import numpy as np

def get_datetime(date_str):
    return datetime.strptime(date_str, '%Y-%m-%d')

def rotate_and_save(input_tuple):
    [image_path, rotation] = input_tuple
    try:
        img = Image.open(image_path)
    except IOError as e:
        print("Bad onboarding file {}. Exception: {}".format(image_path, e))
        return None
    
    if rotation != None:
        img = img.transpose(rotation)
    img.save(image_path, 'JPEG', quality=100)
    return image_path

def save_products(inp):
    [ob_path, _, prod_box, upc], _ = inp
    ob_image = Image.open(ob_path)
    prod_image = ob_image.crop(prod_box)
    file_name = ''.join(random.choice(string.ascii_uppercase + string.digits) for _ in range(10))
    save_path = os.path.join(dir_to_save, '{}.jpg'.format(file_name))
    prod_image.save(save_path, 'JPEG', quality=100)
    return (upc, save_path)

    
class Matcher():
    def __init__(self, local_dir, prod_model_dir, rack_vs_shelf_model_dir):
        self.local_dir = local_dir
        self.ob_dir = os.path.join(self.local_dir, 'ob_images')
        self.prod_model_dir = prod_model_dir
        self.rack_vs_shelf_model_dir = rack_vs_shelf_model_dir
        self.ob_to_tag_map = defaultdict(list)
        
        mongo_credentials = 'mongodb://jameson:d3dcf9ba55@ds023301-a0.mlab.com:23301,ds023301-a1.mlab.com:23301/focal?replicaSet=rs-ds023301'
        mongo_client = MongoClient(mongo_credentials)
        self.db = mongo_client['focal']
        
    def _download(self, file_paths, target_dir, addr_file_dir='./', unzip=False):
        '''
            Description:
                Downloads and saves remote files from 
            Params:
                file_paths: a list of remote gcloud directories to download
                target_dir: The local directory to download to
                addr_file_dir: Where to save the temporary text file for holding remote file_paths
                unzip: If True, will unzip and delete 'images.zip' to target_dir
            Returns:
                None
        '''
        
        if not os.path.exists(target_dir):
            os.makedirs(target_dir)
        file_paths = set(file_paths)
        
        # Collect paths
        remote_addr_file = open(os.path.join(addr_file_dir, 'gym_addrs.txt'), 'a')
        for path in file_paths:
            remote_addr_file.write('{}\n'.format(path))
        remote_addr_file.close()

        # Download
        print("Downloading {} images".format(len(file_paths)))
        dl_res =  os.system('cat {} | gsutil -m cp -I {}'.format(remote_addr_file.name, target_dir))
        if dl_res == 0:
            print("Download succesful.")
        else:
            print("Download failed. Error code {}.".format(dl_res))
        os.system('rm {}'.format(remote_addr_file.name))

        if unzip:
            os.system('unzip -j {} -d {}'.format(os.path.join(target_dir, 'images.zip'), target_dir))
            os.system('rm {}'.format(os.path.join(target_dir,'images.zip')))
            
            
    def _rotate_bbox_90(self, bbox_coords, image_width, image_height):
        '''
        Input parameters:
        - bbox_cords: [x1, y1, w, h]
        - image_width. Source image width
        - image_height: Source image height
        Output:
        List: [x1, y1, x2, y2]
        '''
        
        [x1, y1, w, h] = bbox_coords
        (x2, y2) = (x1+w, y1+h)
        

        new_w_temp = x2 - x1
        new_h_temp = y2 - y1
        new_x1_temp = x1
        new_y1_temp = y1

        # Get new x1 y1 coords
       
        temp = new_w_temp
        new_w = new_h_temp
        new_h = temp

        new_x1 = new_y1_temp
        new_y1 = image_height - (new_x1_temp + new_w_temp)

        return [new_x1, new_y1, new_x1 + new_w, new_y1 + new_h]

    
    def collect_tags(self, store, dates):
        '''
            Description:
                Finds all shelf tags from 'store' and 'dates' and saves them for future reference
            Params:
                store: The store from which we collect tags
                dates: A list of date strings from which we collect tags from 'store' -- 'yyyy-mm-dd'.
            Returns:
                A list of all the rows found in ShelfTag collection
        '''
        
        query_dict = {
            'storeName' : store,
            'date': {'$in' : map(get_datetime, dates)},
            'imagePath': {'$exists': True},
            'upc.actual': {'$exists': True}
        }
        
        return self.collect_tags(query_dict)
        
    def collect_tags(self, shelftag_query_dict, num_to_collect=0):
        '''
            Description:
                Uses 'shelftag_query_dict' to find shelf tags and save them for future reference
            Params:
                shelftag_query_dict: A dictionairy that will be used to query the ShelfTag collection for tags to use
            Returns:
                A list of all the rows found in ShelfTag collection
        '''
        self.tag_rows = list(self.db["ShelfTag"].find(shelftag_query_dict).limit(num_to_collect))
        for row in self.tag_rows:
            ob_path = os.path.join(self.ob_dir, os.path.basename(row['imagePath']))
            self.ob_to_tag_map[ob_path].append(row)
        print("Found {num_tags} tags".format(num_tags=len(self.tag_rows)))
        
        return self.tag_rows
   
    def download_onboarding_images(self, rotation=90):
        '''
            Description:
                Uses 'shelftag_query_dict' to find shelf tags, then downloads all of 
                their corresponding onboarding images and rotates them by 'rotation' degrees.
            Params:
                shelftag_query_dict: A dictionairy that will be used to query the ShelfTag collection for tags to use
                rotation: The degree to rotate the images
            Returns:
                A list of local paths to all of the downloaded onboarding images
        '''
        IMG_ROTATION = {
            0: None,
            90: Image.ROTATE_90,
            180: Image.ROTATE_180,
            270: Image.ROTATE_270
        }[rotation]
        
        remote_ob_paths = [row['imagePath'].replace('https://storage.googleapis.com/', 'gs://') for row in self.tag_rows]

        # Download onboarding images
        self._download(remote_ob_paths, self.ob_dir, addr_file_dir=self.ob_dir)
        all_ob_paths = [os.path.join(self.ob_dir, ob_image_file) for ob_image_file in os.listdir(self.ob_dir)]
        
        pool = Pool(12)
        self.ob_paths = filter(
            lambda x: x != None,
            pool.map(rotate_and_save, zip(all_ob_paths, [IMG_ROTATION]*len(all_ob_paths)))
        )
        pool.terminate()
        
        return self.ob_paths
    
    def _fp_product_model(self, image_paths, model_dir):
        '''
            Description:
                Forward pass images through the product model
            Params:
                image_paths: A list of paths to images to forward pass
                model_dir: The directory containing the product model.pb and properties.json
            Returns:
                A dictionairy mapping from image path to its product bounding boxes
        '''
        product_boxes = dict()
        prod_model_runner = RetinaNetRunner(model_dir)
        print("Forward passing {} images through the product model.".format(len(image_paths)))
        for done, image in enumerate(image_paths):
            if len(image_paths) < 20 or done % (len(image_paths) // 20) == 0:
                print("Done {} / {}".format(done, len(image_paths)))
            batch_result = prod_model_runner.run_model_batched([image])
            bboxes = prod_model_runner.run_pipeline(batch_result)
            bboxes = [map(b.get, ['x1', 'y1', 'x2', 'y2']) for b in bboxes[0]]
            product_boxes[image] = bboxes
        print("Done forward passing.")
        
        return product_boxes

    def find_product_embeddings(self):
        ''' 
            Description:
                Forward pass onboarding images through product detection model and use the results to 
                find matches between product bounding boxes and shelf tag bounding boxes. 
            Params:
                None
            Returns:
                A dictionairy mapping a product UPC to a list of product images.
        '''    
        # Auxiliary function used to map rotated tag bboxes to upright bboxes
        def rotate(ob_path):
            ob_height, ob_width, _ = np.array(Image.open(ob_path)).shape
            def map_func(row):
                row['bbox'] = self._rotate_bbox_90(row['bbox'], ob_width, ob_height)
                return row
            return map_func
    
        results_dir = os.path.join(self.local_dir, 'product_embeddings')
        
        # Forward pass
        product_boxes = self._fp_product_model(self.ob_paths, self.prod_model_dir)
          
        # Build mapping from onboarding image paths to their respective shelf tag bboxes and product bboxes             
        print("Rotating tags")
        bbox_map = {
            ob_path : {
                'product_boxes' : product_boxes[ob_path],
                'tag_rows' : map(rotate(ob_path), self.ob_to_tag_map[ob_path]) 
            }
            for ob_path in self.ob_paths
        }
            
        # Find embedding pairs
        print("Finding embedding pairs")
        resnet_runner = KerasRunner(self.rack_vs_shelf_model_dir)            
        #results = []
        #for done, ob_path in enumerate(bbox_map):
        #    if len(bbox_map) < 20 or done % (len(bbox_map) // 20) == 0:
        #        print("Done {} / {}".format(done, len(bbox_map))) 
                
        #    res = tpr.tag_to_prod(
        #        ob_path,
        #        bbox_map[ob_path]['tag_rows'],
        #        bbox_map[ob_path]['product_boxes'],
        #        resnet_runner,
        #        False
        #    )
        #    results.extend(res)                 
        
        processes = []
        for ob_path in bbox_map:
            inp = [
                ob_path,
                bbox_map[ob_path]['tag_rows'],
                bbox_map[ob_path]['product_boxes'],
                resnet_runner,
                False
            ]
            p = Process(target=tag_to_prod, args=(inp,))
            processes.append(p)
            
        results = [x.start() for x in processes]

        from pprint import pprint 
        pprint(results)
        # Map product upc to product crops, and save product crops in a folder named as the upc
        # Create upc folders
        for [ob_path, _, prod_box, upc], local_dir in results:
            dir_to_save = os.path.join(local_dir, upc)
            if not os.path.exists(dir_to_save):
                os.makedirs(dir_to_save)
        
        
        pool = Pool(12)
        upc_prod_pairs = pool.map(save_products, zip(results, [results_dir]*len(results)))
        pool.terminate()
        
        upc_to_prod_map = defaultdict(list)
        for upc, product in upc_prod_pairs:
            upc_to_prod_map[upc].append(product)

        print("Done.")
        return upc_to_prod_map 
                                   
                                   
                              

