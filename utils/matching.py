#import matplotlib.pyplot as plt
from scipy.spatial import distance as dist
import random
from pymongo import MongoClient
import json
import subprocess
import time
from PIL import Image
import keras
from keras.models import load_model
from keras.preprocessing import image
import tensorflow as tf
import warnings
try:
    import cv2
except ImportError:
    warnings.warn("Open CV Not Imported the following functions will not work: ")
import os
import numpy as np
import math
from modelrunner.frameworks.keras import KerasRunner

from pprint import pprint
from glob import glob
from bson import ObjectId

############################################################################
# Gerneral Purpose Helper Functions
############################################################################
def list2str(L):
    return " ".join(str(x) for x in L)


def str2list(S):
    return map(float, S.split(" "))


def get_iou(bb1, bb2, mode="tag"):
    if mode=="tag":
        bb1 = [bb1[0], bb1[1], bb1[0] + bb1[2], bb1[1] + bb1[3]]
        bb2 = [bb2[0], bb2[1], bb2[0] + bb2[2], bb2[1] + bb2[3]]
    # determine the coordinates of the intersection rectangle
    x_left = max(bb1[0], bb2[0])
    y_top = max(bb1[1], bb2[1])
    x_right = min(bb1[2], bb2[2])
    y_bottom = min(bb1[3], bb2[3])

    if x_right < x_left or y_bottom < y_top:
        return 0.0

    # The intersection of two axis-aligned bounding boxes is always an
    # axis-aligned bounding box
    intersection_area = (x_right - x_left) * (y_bottom - y_top)

    # compute the area of both AABBs
    bb1_area = (bb1[2] - bb1[0]) * (bb1[3] - bb1[1])
    bb2_area = (bb2[2] - bb2[0]) * (bb2[3] - bb2[1])

    # compute the intersection over union by taking the intersection
    # area and dividing it by the sum of prediction + ground-truth
    # areas - the interesection area
    iou = intersection_area / float(bb1_area + bb2_area - intersection_area)
    return iou

# This function is not used in this script, but when you debug the sideways and upright bounding boxes, this might be helpful
def general_bbox_transform(bbox_coords, W_target=None, H_target=None, W_now=None, H_now=None, rotate_degree=90, rotate_only=True):
    '''
    Input parameters:
    - bbox_cords: [x1, y1, x2, y2]
    - W_target: target image width
    - H_target: target image height
    - W_now: current image width
    - H_now: current image height
    - rotate_degree: the amount of counter clockwise rotation needed for Image.transpose. 
                   Normally, from sideway to upright, it should be 90, which indicates PIL.Image.ROTATE_90 function to use
                   Candidate values: 0, 90, 180, 270
    Output:
    List: [x1, y1, x2, y2]
    '''
    
    x1 = bbox_coords[0]
    y1 = bbox_coords[1]
    x2 = bbox_coords[2]
    y2 = bbox_coords[3]
    
    new_w_temp = x2 - x1
    new_h_temp = y2 - y1
    new_x1_temp = x1
    new_y1_temp = y1

    if not rotate_only:
        W_ratio = W_target / float(W_now)
        H_ratio = H_target / float(H_now)
        new_w_temp = new_w_temp * float(W_ratio)
        new_h_temp = new_h_temp * float(H_ratio)
        new_x1_temp = new_x1_temp * float(W_ratio)
        new_y1_temp = new_y1_temp * float(H_ratio)
    
    # Get new x1 y1 coords
    if rotate_degree == 90:
        temp = new_w_temp
        new_w = new_h_temp
        new_h = temp
        
        new_x1 = new_y1_temp
        new_y1 = H_target - (new_x1_temp + new_w_temp)
    
    
    return [new_x1, new_y1, new_x1 + new_w, new_y1 + new_h]

############################################################################
# Stage 1 : Rack vs shelf Helper Functions
############################################################################
def correct_tag_bbox(tag_box, width, height):
    '''
    Get the coordinates for tag crop.
    The goal is to give a "context" of tag when feeding it to the rack vs shelf model
    '''
    x1 = tag_box[0] 
    y1 = tag_box[1] 
    w_bbox = tag_box[2] 
    h_bbox = tag_box[3]
    x2 = x1 + w_bbox
    y2 = y1 + h_bbox
    
    delta_h = max(w_bbox, h_bbox) * 2
    delta_w = max(w_bbox, h_bbox)
    
    new_x1 = x1 - delta_w
    new_y1 = y1 - delta_h
    new_x2 = x1 + max(w_bbox, h_bbox) * 2
    new_y2 = y2 + min(w_bbox, h_bbox) + max(w_bbox, h_bbox) * 2

    if new_x1 < 0:
        new_x1 = 0
    if new_y1 < 0:
        new_y1 = 0
    if new_x2 > width:
        new_x2 = width
    if new_y2 > height:
        new_y2 = height

    box = [new_x1, new_y1, new_x2, new_y2]
    box = map(int, box)
    
    return box


def tag_preprocess(local_path, tag_box):
    '''
    Preprocess the tag crop and pass it into rack vs shelf model
    '''
    image = Image.open(local_path)
    width = np.array(image).shape[1]
    height = np.array(image).shape[0]
    
    x = int(tag_box[0])
    y = int(tag_box[1])
    w = int(tag_box[2])
    h = int(tag_box[3])

    tag_box = map(int, tag_box)
    image = Image.fromarray(cv2.rectangle(np.array(image), 
                                          (x, y), 
                                          (x + w, y + h), 
                                          (0, 255, 0), 
                                           17))
    box = correct_tag_bbox(tag_box, width, height)
    crop = image.crop(box)
    crop = crop.resize((224, 224))
    
    crop_dir = "crops/"
    if not os.path.exists(crop_dir):
        os.makedirs(crop_dir)
    crop_path = crop_dir + local_path.split("/")[-1]
    crop.save(crop_path)
    
    return [crop_path]


def rack_vs_shelf(local_path, tag_box, resnet_runner):
    '''
    Forward pass rack vs shelf model and product the prediction result
    '''
    crop_path = tag_preprocess(local_path, tag_box)
    prediction = resnet_runner.run_model_batched(crop_path)[0]
    
    if prediction[0] > prediction[1]:
        return ["rack", prediction[0]]
    else:
        return ["shelf", prediction[1]]
    

############################################################################
# Stage 2 : Shortest Distance Computation Helper Functions
############################################################################
def get_center_coordinate(bbox, mode):
    '''
    Get every kind of center for 2 kinds of box (prod and tag)
    Tag box follow the format (x, y, w, h)
    Prod box follow the format (x1 ,y1, x2, y2)
    '''
    x1 = bbox[0]
    y1 = bbox[1]
    x2 = bbox[2]
    y2 = bbox[3]
    if x1 == None or x2 == None or y1 == None or y2 == None:
        return
    if mode == "tag_middle":
        center_x = (x1 + x2 + x1) / 2
        center_y = (y1 + y2 + y1) / 2
    elif mode == "tag_bottom":
        center_x = (x1 + x2 + x1) / 2
        center_y = y1 + y2
    elif mode == "tag_top":
        center_x = (x1 + x2 + x1) / 2
        center_y = y1
    elif mode == "prod_bottom":
        center_x = (x1 + x2) / 2
        center_y = y2
    elif mode == "prod_top":
        center_x = (x1 + x2) / 2
        center_y = y1
    elif mode == "prod_middle":
        center_x = (x1 + x2) / 2
        center_y = (y1 + y2) / 2
    else:
        return
    return [center_x, center_y]


def compute_shortest_distance(tag_bbox, prod_bboxes, local_path, distance_threshold, resnet_runner):
    '''
    Get nearest prod box for one tag box
    As well as 
    '''
    tag_bbox_middle_center = get_center_coordinate(tag_bbox, mode="tag_middle")
    if tag_bbox_middle_center == "No":
        return "Not Found"
    tag_bbox_middle_center_x = tag_bbox_middle_center[0]
    tag_bbox_middle_center_y = tag_bbox_middle_center[1]
    
    tag_bbox_bottom_center = get_center_coordinate(tag_bbox, mode="tag_bottom")
    if tag_bbox_bottom_center == "No":
        return "Not Found"
    tag_bbox_bottom_center_x = tag_bbox_bottom_center[0]
    tag_bbox_bottom_center_y = tag_bbox_bottom_center[1]
    
    tag_bbox_top_center = get_center_coordinate(tag_bbox, mode="tag_top")
    if tag_bbox_top_center == "No":
        return "Not Found"
    tag_bbox_top_center_x = tag_bbox_top_center[0]
    tag_bbox_top_center_y = tag_bbox_top_center[1]

    min_distance = 10000
    min_index = 0
    
    tag_type = rack_vs_shelf(local_path, tag_bbox, resnet_runner)
        
    location_counter = 0
    filtered_product_boxes = []
    for i in range(len(prod_bboxes)):
        prod_bbox_bottom_center = get_center_coordinate(prod_bboxes[i], mode="prod_bottom")
        prod_bbox_bottom_center_x = prod_bbox_bottom_center[0]
        prod_bbox_bottom_center_y = prod_bbox_bottom_center[1]
        
        prod_bbox_top_center = get_center_coordinate(prod_bboxes[i], mode="prod_top")
        prod_bbox_top_center_x = prod_bbox_top_center[0]
        prod_bbox_top_center_y = prod_bbox_top_center[1]
        
        prod_bbox_middle_center = get_center_coordinate(prod_bboxes[i], mode="prod_middle")
        prod_bbox_middle_center_x = prod_bbox_middle_center[0]
        prod_bbox_middle_center_y = prod_bbox_middle_center[1]
        
        # Filter out the coordinates that do not have an appropriate relative location
        if tag_type[0] == "shelf" and prod_bbox_bottom_center_y < tag_bbox_bottom_center_y:
            continue
            location_counter += 1
        if tag_type[0] == "rack" and prod_bbox_middle_center_y > tag_bbox_middle_center_y:
            continue
            location_counter += 1
            
        # If there is no satisfactory box, just return None instead of the initialization value
        if location_counter == len(prod_bboxes):
            return "Not Found"
        if tag_type[0] == "shelf":
            distance = math.sqrt((tag_bbox_middle_center_x - prod_bbox_bottom_center_x)**2 + (tag_bbox_middle_center_y - prod_bbox_bottom_center_y)**2)
        if tag_type[0] == "rack":
            distance = math.sqrt((tag_bbox_middle_center_x - prod_bbox_middle_center_x)**2 + (tag_bbox_middle_center_y - prod_bbox_middle_center_y)**2)
            
        
        filtered_product_boxes.append(prod_bboxes[i])
        if distance <= min_distance:
            min_distance = distance
            min_index = i    

    if min_distance > distance_threshold:
        return "Not Found"
    return (tag_bbox, prod_bboxes[min_index], min_distance, tag_type[0], filtered_product_boxes)


############################################################################
# Stage 3 : Color Histogram Helper Functions
############################################################################
def color_histogram_compare(target_prod_img, candidate_prod_img, method):
    '''
    Input : numpy array of image / output of cv2.imread
    Output: score of similarity
    '''
    
    # First calculate the histogram
    target_prod_img = cv2.cvtColor(target_prod_img,cv2.COLOR_RGB2HSV)
    candidate_prod_img = cv2.cvtColor(candidate_prod_img,cv2.COLOR_RGB2HSV)
    target_hist = cv2.calcHist([target_prod_img], [0, 1, 2], None, [8, 8, 8], [0, 256, 0, 256, 0, 256])
    candidate_hist = cv2.calcHist([candidate_prod_img], [0, 1, 2], None, [8, 8, 8], [0, 256, 0, 256, 0, 256])
    
    target_hist = cv2.normalize(target_hist, target_hist).flatten()
    candidate_hist = cv2.normalize(candidate_hist, candidate_hist).flatten()
    
    # Scipy family
    if method == "Euclidean":
        score = dist.euclidean(target_hist, candidate_hist)
        better = "lower"
    if method == "Manhattan":
        score = dist.cityblock(target_hist, candidate_hist)
        better = "lower"
    if method == "Chebyshev":
        score = dist.chebyshev(target_hist, candidate_hist)
        better = "lower"
        
    # CV2 family
    if method == "Correlation":
        score = cv2.compareHist(target_hist, candidate_hist, cv2.HISTCMP_CORREL)
        better = "higher"
    if method == "Chi_Squared":
        score = cv2.compareHist(target_hist, candidate_hist, cv2.HISTCMP_CHISQR)
        better = "lower"
    if method == "Intersect":
        score = cv2.compareHist(target_hist, candidate_hist, cv2.HISTCMP_INTERSECT)
        better = "higher"
    if method == "Bhattacharyya":
        score = cv2.compareHist(target_hist, candidate_hist, cv2.HISTCMP_BHATTACHARYYA)
        better = "lower"
        
    return (score, better)


def same_row_test(nearest_prod_box, all_prod_boxes, offset_threshold):
    '''
    Get all prod boxes which are in the same row with the nearest_prod_box
    '''
    same_row_prod_boxes = []
    target_center = get_center_coordinate(nearest_prod_box, mode="prod_middle")
    for prod_box in all_prod_boxes:
        center = get_center_coordinate(prod_box, mode="prod_middle")
        if center[1] > target_center[1] - offset_threshold and center[1] < target_center[1] + offset_threshold * 2:
            same_row_prod_boxes.append(prod_box)
    return same_row_prod_boxes     


def produce_tag_to_multiple_prods_tuples(local_path,
                                resnet_runner,
                                tag_orig_dict,
                                prod_orig_dict,
                                similarity_threshold=3.5, 
                                offset_threshold=600,
                                distance_threshold=1000,
                                method="Manhattan"):
    '''
    Fine tune all your parameters here
    '''
    tag_to_prod = []
    tag_boxes = tag_orig_dict[local_path]
    prod_bboxes = prod_orig_dict[local_path]
    
    draw = Image.open(local_path)
    img = np.array(draw)
    
    for tag_box in tag_boxes:
        distance_tuple = compute_shortest_distance(tag_box, prod_bboxes, local_path, distance_threshold, resnet_runner)
        if distance_tuple == "Not Found":
            continue
        tag_to_prod.append(distance_tuple)
    tag_to_multiple_prods = tag_to_prod
    tag_to_multiple_prods = []
    for element in tag_to_prod:
        tag_box, nearest_prod_box, min_distance, tag_type, filtered_product_boxes = element
        same_row_prod_boxes = same_row_test(nearest_prod_box, filtered_product_boxes, offset_threshold)
        same_products = []
        same_products.append(nearest_prod_box)
        for prod_box in same_row_prod_boxes:
            nx1 = int(nearest_prod_box[0])
            ny1 = int(nearest_prod_box[1])
            nx2 = int(nearest_prod_box[2])
            ny2 = int(nearest_prod_box[3])
            
            sx1 = int(prod_box[0])
            sy1 = int(prod_box[1])
            sx2 = int(prod_box[2])
            sy2 = int(prod_box[3])
            
            nearest_prod_crop = draw.crop([nx1, ny1, nx2, ny2])
            prod_box_crop = draw.crop([sx1, sy1, sx2, sy2])
            
            similarity_score, better = color_histogram_compare(np.array(nearest_prod_crop), np.array(prod_box_crop), method)
            if better == "lower":
                if similarity_score <= similarity_threshold:
                    same_products.append(prod_box)
            if better == "higher":
                if similarity_score >= similarity_threshold:
                    same_products.append(prod_box)
        tag_to_multiple_prods.append((tag_box, same_products, min_distance, tag_type))
    return tag_to_multiple_prods


############################################################################
# Stage 4 : Avoid competition on product boxes
############################################################################
def get_one_tag_box(prod_box, tag_boxes):
    '''
    When two tag box compete for one prod box, only consider the nearest tag box
    '''
    center_prod_x = get_center_coordinate(prod_box, mode="prod_bottom")[0]
    center_prod_y = get_center_coordinate(prod_box, mode="prod_bottom")[1]
    # Find nearest tag box to this product
    min_distance = 10000
    min_tag_box = None
    for tag_box in tag_boxes:
        center_tag_x = get_center_coordinate(tag_box, mode="tag_middle")[0]
        center_tag_y = get_center_coordinate(tag_box, mode="tag_middle")[1]
        distance = math.sqrt((center_tag_x - center_prod_x)**2 + (center_tag_y - center_prod_y)**2)
        if distance < min_distance:
            min_distance = distance
            min_tag_box = tag_box
    # Find the tag left to nearest tag
    min_left_distance = 1000
    left_box = None
    for tag_box in tag_boxes:
        center_left_tag_x = get_center_coordinate(tag_box, mode="tag_middle")[0]
        center_min_tag_x = get_center_coordinate(min_tag_box, mode="tag_middle")[0]
        if center_min_tag_x < center_left_tag_x:
            continue
        else:
            distance = center_min_tag_x - center_left_tag_x
            if distance < min_left_distance:
                min_left_distance = distance
                left_box = tag_box       
    left_x = get_center_coordinate(left_box, mode="tag_middle")[0]
    left_y = get_center_coordinate(left_box, mode="tag_middle")[1]
    left_tag_distance = math.sqrt((left_x - center_prod_x)**2 + (left_y - center_prod_y)**2)
    if left_tag_distance < 1.5 * min_distance:
        one_tag_box = left_box
    else:
        one_tag_box = min_tag_box
        
    return one_tag_box


def solve_prod_box_compete(tag_to_multiple_prods):
    tag_to_multiple_prods_temp = []
    # Get all prod boxes and the mapping between tag to prods
    prod_boxes = [] # list of list
    tag_to_prods = {} # str to list of list
    for tag_to_multiple_prod in tag_to_multiple_prods:
        tag_box, same_products, _, _ = tag_to_multiple_prod
        prod_boxes += same_products
        prod_boxes = [list(x) for x in set(tuple(x) for x in prod_boxes)]
        tag_to_prods[list2str(tag_box)] = same_products
    
    # Create the mapping between one prod box to multiple tag box
    prod_to_tags = {} # str to list of list
    for prod_box in prod_boxes:
        prod_to_tags[list2str(prod_box)] = []   
    for prod_box in prod_boxes:
        for tag_box, prods in tag_to_prods.iteritems():
            if prod_box in prods:
                prod_to_tags[list2str(prod_box)].append(str2list(tag_box))
    
    # Create the mapping so that one prod box ONLY maps to one tag box        
    prod_to_only_tag = {}
    for prod_box, tag_boxes in prod_to_tags.iteritems():
        min_tag_box = get_one_tag_box(str2list(prod_box), tag_boxes)
        prod_to_only_tag[prod_box] = min_tag_box
        
    # For the prod box that already exist in the mapping of tag_a, delete it from list if it also exist in tag_b
    for tag_to_multiple_prod in tag_to_multiple_prods:
        tag_box, same_products, min_distance, tag_type = tag_to_multiple_prod
        same_products_temp = []
        for element in same_products:
            iou = get_iou(map(int, prod_to_only_tag[list2str(element)]), map(int, tag_box))
            if iou < 0.01:
                continue
            else:
                same_products_temp.append(element)
        tag_to_multiple_prods_temp.append((tag_box, same_products_temp, min_distance, tag_type))
    return tag_to_multiple_prods_temp


def remove_duplicated_tags_for_prediction(tag_to_multiple_prods):
    '''
    Cope with duplicated tag problem for prediction
    '''
    tag_to_multiple_prods_temp = []
    
    tag_box_list = []
    for tag_to_multiple_prod in tag_to_multiple_prods:
        tag_box, _, _, _ = tag_to_multiple_prod
        tag_box_list.append(tag_box)
    
    tag_box_list = [list(x) for x in set(tuple(x) for x in tag_box_list)]
    
    box_count = {}
    for box in tag_box_list:
        box_count[list2str(box)] = 0
        
    for tag_to_multiple_prod in tag_to_multiple_prods:
        add = True
        tag_box, same_products, min_distance, tag_type = tag_to_multiple_prod
        
        for test_box in tag_box_list:
            if get_iou(test_box, tag_box) > 0.65:
                box_count[list2str(test_box)] += 1
                if box_count[list2str(test_box)] > 1:
                    add = False
                    break
        if add == True:
            tag_to_multiple_prods_temp.append((tag_box, same_products, min_distance, tag_type))
    return tag_to_multiple_prods_temp


def post_processing_predition(tag_to_multiple_prods):
    tag_to_multiple_prods_temp = solve_prod_box_compete(tag_to_multiple_prods)
    return remove_duplicated_tags_for_prediction(tag_to_multiple_prods_temp)


############################################################################
# Wrap up the functions above
############################################################################
def plot_mapping_on_image(local_path, resnet_runner, tag_orig_dict, prod_orig_dict, show):
    '''
    It is not just plotting! This is the most important wrap up function that produces the tuple list.
    '''
    tag_font_size = 10
    prod_font_size = 15
    text_margin = 13
    prod_font_color = (255, 255, 255)
    tag_font_color = (244, 255, 22)
    
    draw = Image.open(local_path)
    img = np.array(draw)
    
    predict_tuple_list = []
    
    tag_to_multiple_prods_tuples = post_processing_predition(produce_tag_to_multiple_prods_tuples(local_path, resnet_runner, tag_orig_dict, prod_orig_dict))
    for idx, tag_to_multiple_prods_tuple in enumerate(tag_to_multiple_prods_tuples):
        if tag_to_multiple_prods_tuple == "Not Found":
            return
        tag_bbox, same_products, distance, tag_type = tag_to_multiple_prods_tuple       
        if len(tag_bbox) == 0:
            continue
        x = int(tag_bbox[0])
        y = int(tag_bbox[1])
        w = int(tag_bbox[2])
        h = int(tag_bbox[3])
        
        if show == True:
            caption = str(idx).zfill(2) 
            center_x_tag = int(get_center_coordinate(tag_bbox, mode="tag_middle")[0])
            center_y_tag = int(get_center_coordinate(tag_bbox, mode="tag_middle")[1])
            cv2.circle(img, (int(center_x_tag), int(center_y_tag)), 10, (0, 0, 255), 50)
            cv2.rectangle(img, (x, y), (x + w, y + h), (0, 0, 255), 20)
            cv2.putText(img, caption, (int(x + 50), int(y)), cv2.FONT_HERSHEY_PLAIN, tag_font_size, tag_font_color, text_margin)
        
        if len(same_products) > 0:
            for prod_bbox in same_products: 
                x1 = int(prod_bbox[0])
                y1 = int(prod_bbox[1])
                x2 = int(prod_bbox[2])
                y2 = int(prod_bbox[3])
                
                predict_tuple_list.append((local_path, tag_bbox, prod_bbox))
                if show == True:
                    if tag_type == "shelf":
                        center_x_prod = int(get_center_coordinate(prod_bbox, mode="prod_bottom")[0])
                        center_y_prod = int(get_center_coordinate(prod_bbox, mode="prod_bottom")[1])
                    if tag_type == "rack":
                        center_x_prod = int(get_center_coordinate(prod_bbox, mode="prod_middle")[0])
                        center_y_prod = int(get_center_coordinate(prod_bbox, mode="prod_middle")[1])
                    cv2.circle(img, (int(center_x_prod), int(center_y_prod)), 10, (255, 255, 255), 50)
                    cv2.rectangle(img, (x1, y1), (x2, y2), (255, 0, 0), 20)
                    cv2.putText(img, caption, (int((x1 + x2 ) / 2), int((y1 + y2 ) / 2)), cv2.FONT_HERSHEY_PLAIN, prod_font_size, prod_font_color, text_margin)
    if show == True:
        plt.figure(figsize=(10,10))
        plt.imshow(img)
        plt.show()
    return predict_tuple_list


def produce_predict_tuple_list(predict_tuple_list):
    # Cast predicted coordinates to int
    predict_tuple_list_temp = []
    for tup in predict_tuple_list:
        local_path, tag_box, prod_box = tup
        prod_box = map(int, prod_box)
        predict_tuple_list_temp.append((str(local_path), tag_box, prod_box))
    predict_tuple_list = predict_tuple_list_temp
    
    predict_str_list = list(set(map(str, predict_tuple_list))) 
    temp = []
    for record in predict_str_list:
        tag_box = map(int, map(float, record.split(", [")[1].replace("]", "").split(", ")))
        prod_box = map(int, map(float, record.split(", [")[-1].replace("])", "").split(", ")))
        temp.append((local_path, tag_box, prod_box))
    predict_tuple_list = temp
    
    return predict_tuple_list


def tag_to_prod(local_path, tag_boxes, prod_boxes, resnet_runner, show):
    '''
    Input:
    tag_boxes: list of tag bounding boxes, e.g. [[1,2,3,4], [5,6,7,8]...] follwing the format (x, y, w, h)
    prod_boxes: list of product bounding boxes, e.g. [[1,2,3,4], [5,6,7,8]...] follwing the format (x1, y1, x2, y2)
    
    The function assumes that all bboxes and images are UPRIGHT!
    
    Output:
    A list of tuple which follows (local_path, tag_box, prod_box)
    '''
    output_dict = {}
    output_dict["matched"] = []
    output_dict["unmatched"] = []
    
    tag_box_to_idx = {}
    tag_box_to_sku = {}
    tag_orig_dict = {}
    idx_to_sku = {}
    prod_orig_dict = {}
    tag_orig_dict[local_path] = []
    prod_orig_dict[local_path] = []

    idx_to_prod_boxes = {}
    for idx, row in enumerate(tag_boxes):
        box = row['bbox']
        tag_orig_dict[local_path].append(box)
        tag_box_to_idx[list2str(map(int, box))] = idx
        idx_to_prod_boxes[idx] = []
        
        upc = -1
        if row != None and 'upc' in row and 'actual' in row.get('upc', []):
            upc = row['upc']['actual']
        
        tag_box_to_sku[list2str(map(int, box))] = upc
        idx_to_sku[idx] = upc
    for box in prod_boxes:
        prod_orig_dict[local_path].append(map(int, box))
    
    # Run the pipeline
    predict_tuple_list = []
    predict_tuple_list += plot_mapping_on_image(local_path, resnet_runner, tag_orig_dict, prod_orig_dict, show)
    predict_tuple_list = produce_predict_tuple_list(predict_tuple_list)

    def zip_skus(l):
        local_path, tag_box, prod_box = l
        return [local_path, tag_box, prod_box, tag_box_to_sku[list2str(map(int, tag_box))]]
    
    return map(zip_skus, predict_tuple_list)

    
    # Convert it to format: {"matched" : [[prod, prod], [prod, prod, prod...]], "unmatched" : [prod, prod, prod....]} 
   
    matched_prod_boxes = []

    for tup in predict_tuple_list:
        _, tag_box, prod_box = tup
        matched_prod_boxes.append(prod_box)
        tag_index = tag_box_to_idx[list2str(map(int, tag_box))] 
        idx_to_prod_boxes[tag_index].append(prod_box)

    for idx, tag_box in enumerate(tag_boxes):
        tag_box = map(int, tag_box)
        tag_index = tag_box_to_idx[list2str(map(int, tag_box))]
        output_dict["matched"].append(idx_to_prod_boxes[tag_index])

    for prod_box in prod_boxes:
        if map(int, prod_box) not in matched_prod_boxes:
            output_dict["unmatched"].append(map(int, prod_box))

    return output_dict
