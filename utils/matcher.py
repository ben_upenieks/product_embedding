import json, os, random, string
from collections import defaultdict
from multiprocessing import Pool, Process
from PIL import Image
from modelrunner.frameworks.tensorflow import RetinaNetRunner
from modelrunner.frameworks.keras import KerasRunner
from pymongo import MongoClient
import numpy as np
from matching import tag_to_prod
import datetime
from bson import ObjectId
import ast

def get_datetime(date_str):
    return datetime.datetime.strptime(date_str, '%Y-%m-%d')

# Ancillary top-level functions to be used with multiprocessing.Pool.map
def rotate_and_save(input_tuple):
    [image_path, rotation] = input_tuple
    try:
        img = Image.open(image_path)
    except IOError as e:
        print("Bad onboarding file {}. Exception: {}".format(image_path, e))
        return None
    
    if rotation != None:
        img = img.transpose(rotation)
    img.save(image_path, 'JPEG', quality=100)
    return image_path

def save_products(inp):
    [ob_path, _, prod_box, upc], local_path, date_str = inp
    if upc == -1:
        return None
    dir_to_save = os.path.join(local_path, upc, date_str)
    ob_image = Image.open(ob_path)
    prod_image = ob_image.crop(prod_box)
    file_name = '{}_{}.jpg'.format(
	os.path.basename(ob_path).replace('.jpg', ''),
	''.join(random.choice(string.ascii_uppercase + string.digits) for _ in range(10))
    )
    save_path = os.path.join(dir_to_save, file_name)
    prod_image.save(save_path, 'JPEG', quality=100)
    return (upc, save_path)

    
class Matcher():
    def __init__(self, store, date, local_dir, prod_model_dir, rack_vs_shelf_model_dir, checkpoint_file='checkpoints.txt'):
        self.local_dir = local_dir
        self.ob_dir = os.path.join(self.local_dir, 'ob_images')
        self.prod_model_dir = prod_model_dir
        self.rack_vs_shelf_model_dir = rack_vs_shelf_model_dir
        self.store = store
        self.date_str = date
        self.ob_to_tag_map = defaultdict(list)
        self.ob_to_upc_prod_pair = defaultdict(list)
        
        mongo_credentials = 'mongodb://jameson:d3dcf9ba55@ds023301-a0.mlab.com:23301,ds023301-a1.mlab.com:23301/focal?replicaSet=rs-ds023301'
        mongo_client = MongoClient(mongo_credentials)
        self.db = mongo_client['focal']

	self.already_done = set()
	try:
            with open(checkpoint_file, 'r') as cp:
                for a in cp:
                    ob_id = os.path.basename(a.split(' ')[1]).replace('.jpg', '')
                    self.already_done.add(ob_id)
            print self.already_done # TODO REMOVE
	except IOError as e:
	    print("Could not open checkpoints.txt")
        
    def _download(self, file_paths_list, target_dir, addr_file_dir='./', unzip=False, target_names=None):
        '''
            Description:
                Downloads and saves remote files from 
            Params:
                file_paths: a list of remote gcloud directories to download
                target_dir: The local directory to download to
                addr_file_dir: Where to save the temporary text file for holding remote file_paths
                unzip: If True, will unzip and delete 'images.zip' to target_dir
            Returns:
                None
        '''
        
        if not os.path.exists(target_dir):
            os.makedirs(target_dir)
        file_paths = set(file_paths_list)
        
        # Collect paths
        remote_addr_file = open(os.path.join(addr_file_dir, 'gym_addrs.txt'), 'a')
        for path in file_paths:
            remote_addr_file.write('{}\n'.format(path))
        remote_addr_file.close()

        # Download
        print("Downloading {} images".format(len(file_paths)))
        dl_res =  os.system('cat {} | gsutil -m cp -I {}'.format(remote_addr_file.name, target_dir))
        if dl_res == 0:
            print("Download succesful.")
        else:
            print("Download failed. Error code {}.".format(dl_res))
        os.system('rm {}'.format(remote_addr_file.name))
       
        if unzip:
            os.system('unzip -j {} -d {}'.format(os.path.join(target_dir, 'images.zip'), target_dir))
            os.system('rm {}'.format(os.path.join(target_dir,'images.zip')))
        
        if target_names != None:
            for src_path, dst_name in zip(file_paths_list, target_names):
                src = os.path.join(target_dir, os.path.basename(src_path))
                if not os.path.exists(src):
                    continue
                dst = os.path.join(target_dir, dst_name)
                os.rename(src, dst)
        
            
    def _rotate_bbox_90(self, bbox_coords, image_width, image_height):
        '''
        Input parameters:
        - bbox_cords: [x1, y1, w, h]
        - image_width. Source image width
        - image_height: Source image height
        Output:
        List: [x1, y1, x2, y2]
        '''
        
        [x1, y1, w, h] = bbox_coords
        (x2, y2) = (x1+w, y1+h)
        

        new_w_temp = x2 - x1
        new_h_temp = y2 - y1
        new_x1_temp = x1
        new_y1_temp = y1

        # Get new x1 y1 coords
       
        temp = new_w_temp
        new_w = new_h_temp
        new_h = temp

        new_x1 = new_y1_temp
        new_y1 = image_height - (new_x1_temp + new_w_temp)

        return [new_x1, new_y1, new_w, new_h]

        
    def collect_tags(self, shelftag_query_dict, num_to_collect=0):
        '''
            Description:
                Uses 'shelftag_query_dict' to find shelf tags and save them for future reference
            Params:
                shelftag_query_dict: A dictionairy that will be used to query the ShelfTag collection for tags to use
            Returns:
                A list of all the rows found in ShelfTag collection
        '''

        def before_date(date_str):
            date = get_datetime(date_str)
            def is_good_date(ob_row):
                for ob_date_str in ob_row['dates']:
                    ob_date = get_datetime(ob_date_str)
                    if ob_date > date:
                        return False
                return True

            all_obs = self.db["Onboarding"].find({'storeName' : self.store, 'dates': {'$exists': True}})
            good_ob_ids = set([str(ob['_id']) for ob in all_obs if is_good_date(ob)])
            
            return lambda tag_row: tag_row.get('onboardId', '.') in good_ob_ids

        # Filter out onboarding images we have already processed and onboarding images after March 1, 2018
        # Images after March 1, 2018 may be upright. We will process these seperately 
        self.tag_rows = list(self.db["ShelfTag"].find(shelftag_query_dict).limit(num_to_collect))
        self.tag_rows = filter(lambda x: x['imageId'] not in self.already_done, self.tag_rows)
        self.tag_rows = filter(before_date('2018-03-01'), self.tag_rows)

        for row in self.tag_rows:
            ob_path = os.path.join(self.ob_dir, '{}.jpg'.format(row['imageId']))
            self.ob_to_tag_map[ob_path].append(row)
        print("Found {num_tags} tags".format(num_tags=len(self.tag_rows)))

        return self.tag_rows

   
    def download_onboarding_images(self, rotation=90):
        '''
            Description:
                Uses 'shelftag_query_dict' to find shelf tags, then downloads all of 
                their corresponding onboarding images and rotates them by 'rotation' degrees.
            Params:
                shelftag_query_dict: A dictionairy that will be used to query the ShelfTag collection for tags to use
                rotation: The degree to rotate the images
            Returns:
                A list of local paths to all of the downloaded onboarding images
        '''
        IMG_ROTATION = {
            0: None,
            90: Image.ROTATE_90,
            180: Image.ROTATE_180,
            270: Image.ROTATE_270
        }[rotation]
        
        ob_pairs = [(row['imagePath'].replace('https://storage.googleapis.com/', 'gs://'), '{}.jpg'.format(row['imageId'])) for row in self.tag_rows]
        remote_ob_paths, target_names = zip(*ob_pairs)
        
        # Download onboarding images
        self._download(remote_ob_paths, self.ob_dir, addr_file_dir=self.ob_dir, target_names=target_names)
        all_ob_paths = [os.path.join(self.ob_dir, ob_image_file) for ob_image_file in os.listdir(self.ob_dir)]
        pool = Pool(12)
        self.ob_paths = filter(
            lambda x: x != None,
            pool.map(rotate_and_save, zip(all_ob_paths, [IMG_ROTATION]*len(all_ob_paths)))
        )
        pool.terminate()
        
        return self.ob_paths
    
    def _fp_product_model(self, image_paths, model_dir):
        '''
            Description:
                Forward pass images through the product model
            Params:
                image_paths: A list of paths to images to forward pass
                model_dir: The directory containing the product model.pb and properties.json
            Returns:
                A dictionairy mapping from image path to its product bounding boxes
        '''
        product_boxes = dict()
        prod_model_runner = RetinaNetRunner(model_dir)
        print("Forward passing {} images through the product model.".format(len(image_paths)))
        for done, image in enumerate(image_paths):
            if len(image_paths) < 20 or done % (len(image_paths) // 20) == 0:
                print("Done {} / {}".format(done, len(image_paths)))
	    try:
                batch_result = prod_model_runner.run_model_batched([image])
                bboxes = prod_model_runner.run_pipeline(batch_result)
                bboxes = [map(b.get, ['x1', 'y1', 'x2', 'y2']) for b in bboxes[0]]
                product_boxes[image] = bboxes
	    except IOError as e:
		print e
		continue
        print("Done forward passing.")
        
        return product_boxes

    def find_product_embeddings(self):
        ''' 
            Description:
                Forward pass onboarding images through product detection model and use the results to 
                find matches between product bounding boxes and shelf tag bounding boxes. 
            Params:
                None
            Returns:
                A dictionairy mapping a product UPC to a list of product images.
        '''    
        # Auxiliary function used to map rotated tag bboxes to upright bboxes
        def rotate(ob_path):
            ob_height, ob_width, _ = np.array(Image.open(ob_path)).shape
            def map_func(row):
                row['bbox'] = self._rotate_bbox_90(row['bbox'], ob_width, ob_height)
                return row
            return map_func
    
        results_dir = os.path.join(self.local_dir, 'product_embeddings')
        if not os.path.exists(results_dir):
            os.makedirs(results_dir)
        
        # Forward pass
        product_boxes = self._fp_product_model(self.ob_paths, self.prod_model_dir)
        
        # Build mapping from onboarding image paths to their respective shelf tag bboxes and product bboxes             
        print("Rotating tags")
        bbox_map = {
            ob_path : {
                'product_boxes' : product_boxes[ob_path],
                'tag_rows' : map(rotate(ob_path), self.ob_to_tag_map[ob_path]) 
            }
            for ob_path in self.ob_paths
        }

        # Find embedding pairs
        print("Finding embedding pairs")
        resnet_runner = KerasRunner(self.rack_vs_shelf_model_dir)            
        results = []
	with open('checkpoints.txt', 'a') as cp:
            for done, ob_path in enumerate(bbox_map):
                if len(bbox_map) < 20 or done % (len(bbox_map) // 20) == 0:
                    print("Done {} / {}".format(done, len(bbox_map))) 
                try:               
                    res = tag_to_prod(
                        ob_path,
                        bbox_map[ob_path]['tag_rows'],
                        bbox_map[ob_path]['product_boxes'],
                        resnet_runner,
                        False
                    )
                    if res == None or res == []:
                        continue
                    res = filter(lambda x: x!=None and x != [], res)
		    print res
                    results.extend(res)                 
                    for r in res:
                        a,b,c,d = r
                        cp.write('{} {} {} {} {}\n'.format(done, a,b,c,d))
                except:
                    print "Error"
                
        # Map onboarding images to a list of (upc, product_box) pairs
        for [ob_path, _, prod_box, upc] in results:
            self.ob_to_upc_prod_pair[ob_path].append((upc, prod_box))
        # Map product upc to product crops, and save product crops in a folder named as the upc
        # Create upc folders
        for [ob_path, _, prod_box, upc] in results:
            if upc == None:
                upc = -1
            if upc == -1:
                continue
            dir_to_save = os.path.join(results_dir, upc, self.date_str)
            if not os.path.exists(dir_to_save):
                os.makedirs(dir_to_save)
        
        
        pool = Pool(12)    
        upc_prod_pairs = pool.map(save_products, zip(results, [results_dir]*len(results), [self.date_str]*len(results)))
        pool.terminate()
        upc_prod_pairs = filter(lambda x: x != None, upc_prod_pairs)
        
        upc_to_prod_map = defaultdict(list)
        for upc, product in upc_prod_pairs:
            upc_to_prod_map[upc].append(product)

        print("Done.")
        return upc_to_prod_map 
                                   
        
    def upload_product_embeddings(self, gym_bucket='gs://focal-training-gyms/product_embedding/'):
        print("Uploading product embeddings.")
        remote_path = os.path.join(gym_bucket, self.store)
        local_dir = os.path.join(self.local_dir, 'product_embeddings', '')
        
        for upc in os.listdir(local_dir):
            dl_res = os.system('gsutil -m cp {} {}'.format(
                os.path.join(local_dir, upc, self.date_str, '*'),
                os.path.join(remote_path, upc, self.date_str, '')
            ))
            if dl_res != 0:
                print("Product embedding upload failed for {}, with error code: {}".format(upc, dl_res))
        print("Done uploadling")
            
            
    def upload_template_matchings(self, gym_bucket='gs://focal-training-gyms/template_matching/', image_size=(1000,1000), overwrite=False):
        ''' 
        Resize onboarding images to 1000x1000 and upload to training gym, along with a json file
        mapping onboarding image to a list of (upc, product_bbox) pairs
        
        Params:
            gym_bucket: the base gym bucket to upload 
            image_size: an tuple specifying the size at which to resize the onboarding images
            overwrite: whether or not to overwrite any data already in the training gym.
        '''
        print("Uploading template matchings")

        remote_path = os.path.join(gym_bucket, self.store, self.date_str, '')
        if remote_path[-1] != '/':
            remote_path += '/'
        
        resized_dir = os.path.join(self.local_dir, 'ob_resized')
        if not os.path.exists(resized_dir):
            os.makedirs(resized_dir)
        resized_results = dict()
        
        local_json_path = os.path.join(self.local_dir, 'labels.json')
        # Will pull existing labels.json and add to it
        if not overwrite:
            labels_path = os.path.join(remote_path, 'labels.json')
            file_exists = os.system('gsutil stat {}'.format(labels_path))
            if file_exists == 0:    
                dl_res = os.system('gsutil -m cp {} {}'.format(labels_path, self.local_dir))
                if dl_res != 0:
                    print("Could not download labels.json from {}. Error code: {}.".format(remote_path, dl_res))
                else:
                    with open(local_json_path, 'r') as j:
                        resized_results = json.load(j)
                        
        print("Rotating and resizing onboarding images")           
	print(self.ob_to_upc_prod_pair)
        for done, (ob_path, pairs) in enumerate(self.ob_to_upc_prod_pair.items()):
            nump = len(self.ob_to_upc_prod_pair)
            if nump < 20 or done % (nump // 20) == 0:
                print("Done {} / {}".format(done, nump)) 
                
            ob_img = Image.open(ob_path)
            ob_height, ob_width, _ = np.array(ob_img).shape
            target_width, target_height = image_size
            height_scale = float(target_height)/ob_height
            width_scale = float(target_width)/ob_width
           
            resized_path = os.path.join(resized_dir, os.path.basename(ob_path))
            for upc, product_bbox in pairs:
                [x1,y1,x2,y2] = product_bbox
                x1 *= width_scale
                y1 *= height_scale
                x2 *= width_scale
                y2 *= height_scale
                
                ob_name = os.path.basename(ob_path)
                if ob_name not in resized_results:
                    resized_results[ob_name] = list()
                resized_results[ob_name].append((upc, map(int, [x1,y1,x2,y2])))
            
            # resize and save onboarding images
            ob_img = ob_img.resize(image_size)
            ob_img.save(resized_path, 'JPEG', quality=100)
            os.system('rm {}'.format(ob_path))
            
        
        
        # Upload
        with open(local_json_path, 'w') as d:
            json.dump(resized_results, d)
        
        os.system('gsutil -m cp {} {}'.format(local_json_path, remote_path))
        os.system('gsutil -m cp {} {}'.format(os.path.join(resized_dir, '*'), remote_path))
        
        print("Done uploading.")
        return resized_results

            
    def clean(self):
        ''' Deletes all left-over data. '''
        os.system('rm -r {}'.format(self.local_dir))
        print("Done cleaning")
        
        
    def _parse_checkpoint_file(self, checkpoint_file):
        results = []
        ob_to_data = defaultdict(list)
        with open('checkpoints.txt', 'r') as cp:
            try: 
                for line_str in cp:
                    line = line_str.strip('\n').split(' ')
                    ob_path = line[1]
                    upc = line[-1]

                    lists = line[2:-1]
                    lists = ' '.join(lists)
                    lists = '], ['.join(lists.split('] ['))

                    tag_box, prod_box = ast.literal_eval(lists)
                    ob_to_data[ob_path].append([upc, prod_box])
                    results.append([ob_path, tag_box, prod_box, upc])
            except SyntaxError as e:
                print e

        return results, ob_to_data

 

































































