##########
# Config #
##########

store = 'walmart_guelph'
prod_model_dir='/root/notebooks/models/product'
rack_vs_shelf_model_dir='/root/notebooks/models/rvs/'
# Optionally, set the dates below.

##########
import os
os.environ["CUDA_VISIBLE_DEVICES"] = '0'
from utils.matcher import Matcher
from pymongo import MongoClient
mongo_credentials = 'mongodb://jameson:d3dcf9ba55@ds023301-a0.mlab.com:23301,ds023301-a1.mlab.com:23301/focal?replicaSet=rs-ds023301'
mongo_client = MongoClient(mongo_credentials)
db = mongo_client['focal']

def run(matcher, query_dict):
    matcher.collect_tags(query_dict) #e
    matcher.download_onboarding_images()
    matcher.find_product_embeddings()
    matcher.upload_product_embeddings()
    matcher.upload_template_matchings()
    matcher.clean()
    

dates = db["ShelfTag"].distinct('date', {'storeName': store})
date_strs = map(lambda i: i.strftime('%Y-%m-%d'), dates)


for date, date_str in zip(dates, date_strs):
    matcher = Matcher(
        store=store,
        date=date_str,
        local_dir = './matches/',
        prod_model_dir = prod_model_dir,
        rack_vs_shelf_model_dir = rack_vs_shelf_model_dir
    )
    
    query_dict = {
        'date': date,
        'storeName': matcher.store,
        'imagePath': {'$exists': True},
        'imageId': {'$exists': True},
	'upc': {'$exists' : True, '$nin': [None, '', {}, []]},
	'upc.actual': {'$nin': [None, '']}
    }
    
    run(matcher, query_dict) 
